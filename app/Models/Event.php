<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\Sport;

class Event extends Model
{
    use HasFactory;

    public $fillable = [
        'title',
        'description',
        'start_date',
        'user_id',
        'location'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'event_user');
    }

    public function sports()
    {
        return $this->belongsToMany(Sport::class, 'event_sport');
    }
}
