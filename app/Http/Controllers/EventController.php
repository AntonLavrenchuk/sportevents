<?php

namespace App\Http\Controllers;

use App\Models\Event;
use App\Models\User;
use App\Models\Sport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events = Event::with('user')->get();
        $users = User::with('events')->get();

        return view('events.index', [ 'events' => $events, 'users' => $users ]);
    }

    public function my_events()
    {
        $events = Event::with('user')->get();
        $users = User::with('events')->get();

        return view('events.my_events', [ 'events' => $events, 'users' => $users ]);
    }

    public function participate(Event $event)
    {
        $alreadyParticipate = in_array(   
            Auth::id(), 
            array_map(
                function($user) {
                    return $user["id"];
                }, 
                $event->users->toArray() ) );

        if( !$alreadyParticipate )
        {
            $event->users()->attach(Auth::id());
        }   

        return redirect('events');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $sports = Sport::all();

        return view('events.create', [ 'sports' => $sports ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $event = new Event();
        
        $event->fill( $request->all() );
        $event->save();
        $event->sports()->attach($request->sports);
        
        return redirect('events');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function show(Event $event)
    {
        return view('events.show', ['event' => $event]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function edit(Event $event)
    {
        return view('events.edit', ['event' => $event]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Event $event)
    {
        $request->validate([
            'title' => 'required|max:50'
            ]);
                    
        $event->fill( $request->all() );
        $event->save();

        return redirect('events');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function destroy( Event $event )
    {
        $sportIdArray = [];

        foreach( $event->sports as $sport )
        {
            $sportIdArray[] = $sport->id;
        }
        
        $event->sports()->detach( $sportIdArray );

        $event->delete();

        return redirect('events');
    }
}
