EDIT

@if (Auth::id() != $event->user_id )
    <script>window.location = "/";</script>
@endif

<form method="POST" action="{{ route( 'events.update', $event ) }}">
    @csrf
    @method('PUT')

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <input type="text" name="title" value="{{ $event->title }}" /> <br>
    <textarea type="text" name="description"> {{ $event->description }} </textarea>

    <input type="submit" />

</form>