<h2>
    Events list    
</h2>

<ul>
    @foreach ($events as $event)
        <li> 
            <p><a href="{{ route( 'events.show', $event ) }}">{{ $event->title }}</a></p>
            <b>Owner:</b>
            <ul>
                <li> {{ $event->user->name }} </li>
            </ul>
            <b>Sports:</b>
            <ul>
                @foreach ($event->sports as $sport)
                    <li> {{ $sport->title }} </li>
                @endforeach
            </ul>
            <b>Users:</b>
            <ul>
                @foreach ($event->users as $user)
                    <li> {{ $user->name }} </li>
                @endforeach
            </ul>
            @php
                $alreadyParticipate = in_array(   Auth::id(), 
                            array_map(
                                function($user) {
                                    return $user["id"];
                                }, 
                                $event->users->toArray()) );
            @endphp
            @if( !$alreadyParticipate )
                <p><a href="{{ route( 'events.participate', $event ) }}">I'm in!</a></p>
            @endif
        </li>
    @endforeach
</ul>