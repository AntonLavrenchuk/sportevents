<head>
    <meta charset="UTF-8">
    <title>Example</title>
    <script type="text/javascript"
            src="https://maps.googleapis.com/maps/api/js?key="></script>
    <script src="https://unpkg.com/location-picker/dist/location-picker.min.js"></script>
    <style type="text/css">
        #map {
        width: 100%;
        height: 480px;
        }
    </style>
</head>

<form method="POST" action="{{ route( 'events.store' ) }}">

    @csrf
    <input type="text" name="title"/> <br>
    <textarea name="description"></textarea>
    <select name="sports[]" multiple> 
            @foreach ($sports as $sport)
              <option value={{ $sport->id }}> {{ $sport->title }} </option>
            @endforeach
    </select>

    <input type="date" name="start_date"
        min='{{date("Y-m-d")}}'>

    <input type="hidden" id="location"  name="location" />
    <input type="hidden" name="user_id" value="{{Auth::id()}}" />

    <input type="submit" />

</form>


<div id="map"></div>

<script>

    var locationInput = document.getElementById('location');

  // Initialize locationPicker plugin
    var lp = new locationPicker('map', {
    setCurrentPosition: true, // You can omit this, defaults to true
    }, {
    zoom: 15 // You can set any google map options here, zoom defaults to 15
    });

  // Listen to map idle event, listening to idle event more accurate than listening to ondrag event
    google.maps.event.addListener(lp.map, 'idle', function (event) {
    // Get current location and show it in HTML
    var location = lp.getMarkerPosition();
    
    locationInput.value = '{"lat":'+location.lat+', "lon":'+location.lng+"}";
    });
</script>

