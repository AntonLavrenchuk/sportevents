<h2>
    My events    
</h2>

<ul>
    @foreach ($events as $event)
        @if (Auth::id() == $event->user_id)
            <li> 
                <p><a href="{{ route( 'events.show', $event ) }}">{{ $event->title }}</a></p>
                <b>Owner:</b>
                <ul>
                    <li> {{ $event->user->name }} </li>
                </ul>
                <b>Sports:</b>
                <ul>
                    @foreach ($event->sports as $sport)
                        <li> {{ $sport->title }} </li>
                    @endforeach
                </ul>
                <b>Users:</b>
                <ul>
                    @foreach ($event->users as $user)
                        <li> {{ $user->name }} </li>
                    @endforeach
                </ul>
            </li>
        @endif
    @endforeach
</ul>