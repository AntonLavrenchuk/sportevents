<h1> {{ $event->title }} </h1>

(<a href="{{ route( 'events.edit', $event ) }}"> edit </a>)

<form method="POST" action="{{route('events.destroy', $event) }}">
@method('DELETE')
@csrf
    <button type="submit">Delete</button>
</form>