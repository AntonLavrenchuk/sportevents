<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\EventController;
use App\Http\Controllers\SportController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'home'])->name('home');
Route::get('/dashboard', function(){ return view('dashboard'); })->name('dashboard');

Route::get('/events/my_events',  [EventController::class, 'my_events'])->name('events.my_events');
Route::get('/events/participate/{event}',  [EventController::class, 'participate'])->name('events.participate');

Route::resource( 'events', EventController::class );

Route::resource( 'sports', SportController::class );

// Route::get('sports/{sport:slug}', [SportController::class, 'show'])->name('sports.show');
require __DIR__.'/auth.php';
