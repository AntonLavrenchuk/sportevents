## SportEvents

#1
- Login
- Register
- Logout

#2
- Add event
    - Time
    - Kind of sport
    - Location
- Join event

#3
- Add event
    - Apply ( or not ) users
    - Max amount of users
    - Location on map
- Join event
    - Search activity by
        - Location
        - Time
        - Kind of sport
- Profile
    - Current activities

#4
- Chat
- Profile
    - User grade
    - Archive activities
